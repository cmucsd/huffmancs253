'''
Created on Apr 24, 2012

@author: cmaier
'''
from string import ascii_letters

def rot13(s):
    instring= ascii_letters
    outstring= ascii_letters[13:26]+ascii_letters[:13]+ascii_letters[39:]+ascii_letters[26:39]
    rots= reduce(lambda s,c : s+c, [outstring[instring.find(c)] if instring.find(c)>-1 else c for c in s])
    return rots

print rot13("Verhuelsdonck!")
