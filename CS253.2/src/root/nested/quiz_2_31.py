# -----------
# User Instructions
# 
# Modify the valid_day() function to verify 
# whether the string a user enters is a valid 
# day. If the passed in parameter 'day' 
# is not a valid day, return None. 
# If 'day' is a valid day, then return 
# the day as a number. Don't worry about 
# months of different length. Assume a day 
# is valid if it is a number between 1 and 
# 31.
#

def valid_day(day):
    if day and day.isdigit():
        d= int(day)
        if d > 0 and d < 32:
            return d

print valid_day('0') #=> None    
print valid_day('1') #=> 1
print valid_day('15') #=> 15
print valid_day('500') #=> None
print valid_day('schtonk')

