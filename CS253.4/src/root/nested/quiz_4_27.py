import hmac

# Implement the hash_str function to use HMAC and our SECRET instead of md5
SECRET = 'imsosecret'
def hash_str(s):
    hashed= hmac.new(SECRET, s).hexdigest()
    return hashed

def make_secure_val(s):
    return "%s|%s" % (s, hash_str(s))

def check_secure_val(h):
    val = h.split('|')[0]
    if h == make_secure_val(val):
        return val

print check_secure_val(make_secure_val('Oynck!'))
print check_secure_val('Oynck!,00000000000000000000000000000000')

