import random
import string

# implement the function make_salt() that returns a string of 5 random
# letters use python's random module.
# Note: The string package might be useful here.

def make_salt():
    salt= ''.join(random.choice(string.ascii_letters) for i in range(5))
    return salt

for j in range(12):
    print make_salt()
