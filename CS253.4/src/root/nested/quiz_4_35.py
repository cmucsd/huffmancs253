import random
import string
import hashlib

def make_salt():
    return ''.join(random.choice(string.letters) for x in xrange(5))

# implement the function make_pw_hash(name, pw) that returns a hashed password 
# of the format: 
# HASH(name + pw + salt),salt
# use sha256

def make_pw_hash(name, pw):
    salz= make_salt()
    eingepoekeltes= name + pw + salz
    hashed= hashlib.sha256(eingepoekeltes).hexdigest()
    return '%s,%s' % (hashed, salz)

for i in range(4):
    print make_pw_hash('Leck mich','im Abendrot')
