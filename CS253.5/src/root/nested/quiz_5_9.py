'''
udacity CS253 week 5 quiz 9: parse XML

Created on May 15, 2012

@author: cmaier
'''
import urllib2
from xml.dom import minidom

page= urllib2.urlopen('http://www.nytimes.com/services/xml/rss/nyt/GlobalHome.xml')
parsed= minidom.parseString(page.read())
items= parsed.getElementsByTagName('item')
print len(items)
pass