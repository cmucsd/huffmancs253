from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
import cgi

class BirthdayPage(webapp.RequestHandler):
    form="""
        <form method="post">
            What is your birthday?
            <br>
            <label>Day<input type="text" name="day" value="%(day)s"></label>
            <label>Month<input type="text" name="month" value="%(month)s"></label>
            <label>Year<input type="text" name="year" value="%(year)s"></label>
            <div style="color: red">%(error)s</div>
            <br>
            <br>
            <input type="submit">
        </form>"""
    months = ['January',
              'February',
              'March',
              'April',
              'May',
              'June',
              'July',
              'August',
              'September',
              'October',
              'November',
              'December']          
    def valid_day(self, day):
        if day and day.isdigit():
            d= int(day)
            if d > 0 and d < 32:
                return d
    def valid_month(self, month):
        mc= month.capitalize()
        if mc in self.months:
            result= mc
        else:
            result= None
        return result
    def valid_year(self, year):
        if year and year.isdigit():
            y= int(year)
            if y > 1900 and y < 2020:
                return y

    def escape_html(self, s):
        return cgi.escape(s, quote= True)

    def write_form(self, error= "", day= "", month= "", year= ""):
        self.response.out.write(self.form % {"error" : error, "day" : day, "month": month, "year" : year})
        
    def get(self):
        self.write_form()
    def post(self):
        user_day= self.request.get('day')
        user_month= self.request.get('month')
        user_year= self.request.get('year')
        day= self.valid_day(user_day)
        month= self.valid_month(user_month)
        year= self.valid_year(user_year)

        if not (day and month and year):
            self.write_form("Looks wrong to me.", self.escape_html(user_day), self.escape_html(user_month), self.escape_html(user_year))
        else:
            self.redirect('/thanks')

class ThanksHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write("Valid date.")
    

class CatPage(webapp.RequestHandler):
    def get(self):
        self.response.out.write("""<html>
            <head>
                <title>Hello, Udacity!</title>
            </head>
            <body>
                <img src="http://24.media.tumblr.com/gbPxtaI5qr0jsjp9CtYQ2EICo1_400.gif">
            </body>
        </html>""")

application = webapp.WSGIApplication(
    [('/', CatPage),('/birthday', BirthdayPage),('/thanks', ThanksHandler),('/cat', CatPage)], debug=True)


def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
