'''
Udacity CS253 week 2 homework 1: rot13 encoder

Created on Apr 24, 2012

@author: cmaier
'''
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
import cgi
from string import ascii_letters
#import codecs

class rot13Page(webapp.RequestHandler):
    def rot13(self,s):
        instring= ascii_letters
        outstring= ascii_letters[13:26]+ascii_letters[:13]+ascii_letters[39:]+ascii_letters[26:39]
        rots= "".join([outstring[instring.find(c)] if instring.find(c)>-1 else c for c in s])
        return rots

    pagehtml="""
        <form method="post">
          <textarea name="text">%(text)s</textarea>
          <br>
          <input type="submit" value="Dixit C&aelig;sar">
        </form>"""

    def get(self):
        self.response.out.write(self.pagehtml % {"text" : ""})

    def post(self):
        plaintext= self.request.get('text')
        #rottext= codecs.encode(plaintext, 'rot13')
        escapetext= cgi.escape(self.rot13(plaintext), quote= True)
        #self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write(self.pagehtml % {"text" : escapetext})

application = webapp.WSGIApplication(
    [('/hw_2_1', rot13Page)], debug=True)


def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()