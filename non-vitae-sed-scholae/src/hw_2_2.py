'''
Udacity CS253 week 2 homework 2: sign-up page

Created on Apr 24, 2012

@author: cmaier
'''
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
import re
import cgi

class PasswordQuery(webapp.RequestHandler):
    form='''<head>
        <title>Sign Up</title>
        <style type="text/css">
          .label {text-align: right}
          .error {color: red}
        </style>
    
      </head>
    
      <body>
        <h2>Signup</h2>
        <form method="post">
          <table>
            <tr>
              <td class="label">
                Username
              </td>
              <td>
                <input type="text" name="username" value="%(uid)s">
              </td>
              <td class="error">
                %(bad_uid)s
              </td>
            </tr>
    
            <tr>
              <td class="label">
                Password
              </td>
              <td>
                <input type="password" name="password" value="">
              </td>
              <td class="error">
                %(bad_pwd)s
              </td>
            </tr>
    
            <tr>
              <td class="label">
                Verify Password
              </td>
              <td>
                <input type="password" name="verify" value="">
              </td>
              <td class="error">
                %(pwd_mismatch)s
              </td>
            </tr>
    
            <tr>
              <td class="label">
                Email (optional)
              </td>
              <td>
                <input type="text" name="email" value="%(email)s">
              </td>
              <td class="error">
                %(bad_email)s
              </td>
            </tr>
          </table>
    
          <input type="submit">
        </form>
      </body>
    '''
    def write_form(self, uid="", email="", bad_uid="", bad_pwd="", pwd_mismatch="", bad_email=""):
        self.response.out.write(self.form % {"uid" : uid, "email" : email, 
                                             "bad_uid" : bad_uid, "bad_pwd" : bad_pwd, "pwd_mismatch" : pwd_mismatch, "bad_email" : bad_email})

    validations= [re.compile(regexp) for regexp in (r"^[a-zA-Z0-9_-]{3,20}$", r"^.{3,20}$", r"^[\S]+@[\S]+\.[\S]+$")]
    def validate(self, s, regexp):
        return regexp.match(s)

    def get(self):
        self.write_form()
    
    def post(self):
        (uid_in, pwd_in, pwd2_in, email_in)= [self.request.get(var) for var in ("username", "password", "verify", "email")]
        (uid_valid, pwd_valid, email_matches)= [self.validate(s, regexp) for (s, regexp) in zip((uid_in, pwd_in, email_in),self.validations)]
        email_valid= email_matches or email_in == ""
        pwd_match= pwd2_in == pwd_in
        (uid, email)= [cgi.escape(s, quote=True) for s in (uid_in, email_in)]
        if (uid_valid and pwd_valid and email_valid and pwd_match):
            self.redirect('/hw_2_2_pass?username=%(uid)s' % {"uid" : uid})
        else:
            bad_uid= "" if uid_valid else "That's not a valid username."
            bad_pwd= "" if pwd_valid else "That's not a valid password."
            pwd_mismatch= "" if pwd_match else "Your passwords didn't match."
            bad_email= "" if email_valid else "That's not a valid email."
            #self.response.headers['Content-Type'] = 'text/plain'
            self.write_form(uid, email, bad_uid, bad_pwd, pwd_mismatch, bad_email)

class PasswordAccepted(webapp.RequestHandler):
    text='''<head>
            <title>Unit 2 Signup</title>
      </head>
      <body>
        <h2>Welcome, %(uid)s!</h2>
      </body>'''
    def get(self):
        uid=self.request.get("username")
        self.response.out.write(self.text % {"uid" : uid})

application = webapp.WSGIApplication([('/hw_2_2', PasswordQuery), ('/hw_2_2_pass', PasswordAccepted)], debug=True)


def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
