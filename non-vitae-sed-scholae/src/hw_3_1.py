'''
Udacity CS253 week 3 homework 1: blog

Created on May 8, 2012

@author: cmaier
'''

import webapp2
import jinja2
import os
from google.appengine.ext import db
from google.appengine.api import memcache
import logging

rootpath='/hw_6'
template_dir= os.path.join(os.path.dirname(__file__),'templates')
jinja_env= jinja2.Environment(loader= jinja2.FileSystemLoader(template_dir), autoescape= True)

class MemCacheHandler():
    listkey= 'entries'
    retries= 64
    def cache_entry(self, postid, update=False):
        c= memcache.Client()
        idstring=str(postid)
        entries= c.gets(self.listkey)
        logging.error('looking for entry %s in memcache entries: %s' % (idstring, repr(entries)))
        if entries is None:
            logging.error('no keys in memcache, initializing empty list')
            c.set(self.listkey,[])
            entries= c.gets(self.listkey)
        if idstring not in entries:
            for t in range(self.retries):
                entries.append(idstring)
                if c.cas(self.listkey, entries):
                    break
                entries= c.gets(self.listkey)
        logging.error('looking for entry %s in memcache' % (idstring))
        blogentry= memcache.get(idstring)
        if blogentry is None or update:
            logging.error('retrieving from database and memcaching')
            blogentry= BlogEntry.get_by_id(postid)
            dummyentry= BlogEntry(subject= 'dummy', content= 'entry')
            blogentry.last_modified= dummyentry.last_modified
            c.set(idstring, blogentry)
        return blogentry
    def cache_contents(self):
        c= memcache.Client()
        entries= c.gets(self.listkey)
        return entries
    def flush_cache(self):
        memcache.flush_all()
    def elapsed(self, td):
        return td.seconds+24*3600*td.days

class BlogEntry(db.Model):
    '''database item for blog entry'''
    subject= db.StringProperty(required= True)
    content= db.TextProperty(required= True)
    posted= db.DateTimeProperty(auto_now_add= True)
    last_modified= db.DateTimeProperty(auto_now= True)
    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)
    def render(self):
        self._render_text= self.content.replace('\n', '<br>')
        return self.render_str("post.html", p= self)

class Handler(webapp2.RequestHandler):
    '''some utility functions for page handlers on top of webapp2.RequestHandler'''
    def write(self, *text, **kw):
        '''write text; default format is HTML'''
        self.response.out.write(*text, **kw)
    def render_str(self, template, **params):
        '''render Jinja template'''
        t= jinja_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        '''write Jinja template'''
        self.write(self.render_str(template, **kw))

class NewEntryHandler(Handler):
    '''handler for new blog entry'''
    def render_page(self, subject= "", content= "", err_subject= "", err_content= ""):
        self.render("newpost.html", subject= subject, content= content, err_subject= err_subject, err_content= err_content)       

    def get(self):
        self.render_page()

    def post(self):
        (subject, content)= [self.request.get(var) for var in ('subject', 'content')]
        if subject and content:
            blogentry= BlogEntry(subject= subject, content= content)
            blogentry.put()
            postid= blogentry.key().id()
            self.write('%(id)s Da schtinkt einer mit dem Schtonken!' % {'id' : postid})
            self.redirect('%(rootpath)s/%(id)d' % {'rootpath': rootpath, 'id' : postid})
        else:
            err_subject= "" if subject else "This entry needs a subject"
            err_content= "" if content else "This entry needs content"
            self.render_page(subject, content, err_subject, err_content)

class BlogEntryHandler(Handler, MemCacheHandler):
    '''handler for blog entry display'''
    def get_entry(self, idstring):
        '''get blog entry by ID string; return (ID number, blog entry object)'''
        postid= int(idstring)
        blogentry= self.cache_entry(postid)
        return postid, blogentry

    def get(self, idstring):
        postid, blogentry= self.get_entry(idstring)
        dummyentry= BlogEntry(subject= 'dummy', content= 'entry')
        queried= self.elapsed(dummyentry.last_modified - blogentry.last_modified)
        logging.error('now: %s last modified: %s' % (dummyentry.last_modified, blogentry.last_modified))
        self.render("blog_entry.html", path='%(rootpath)s/' % {'rootpath' : rootpath}, blogid= postid, entry= blogentry, queried= queried)

class MainBlogHandler(BlogEntryHandler, MemCacheHandler):
    '''handler for main blog page'''
    def get(self):
        posts= self.cache_contents()
        logging.error('posts: %s' % (repr(posts)))
        if posts is None:
            posts= db.GqlQuery('SELECT * from BlogEntry ORDER BY posted DESC')
            entries= [(blogentry.key().id(), blogentry) for blogentry in posts]
            logging.error('database entries: %s' % (repr(entries)))
            for entry in entries:
                self.cache_entry(entry[0])
            posts= self.cache_contents()
        logging.error('posts: %s' % repr(posts))
        if not posts:
            posts= []
        entries= [(int(idstring), memcache.get(idstring)) for idstring in posts]
        entries.sort(key= lambda tup: tup[-1].posted, reverse= True)
        logging.error('memcache entries: %s' % (repr(entries)))
        dummyentry= BlogEntry(subject= 'dummy', content= 'entry')
        queried= self.elapsed(dummyentry.last_modified - max(blogentry[-1].last_modified for blogentry in entries)) if entries else 0
        self.render("blog.html", path='%(rootpath)s/' % {'rootpath' : rootpath}, entries= entries, login= 'login', signup= 'signup', queried= queried)

class FlushHandler(Handler, MemCacheHandler):
    def get(self):
        c= memcache.Client()
        logging.error('Flushing')
        c.flush_all()
        self.redirect('%(rootpath)s/' % {'rootpath': rootpath})

app = webapp2.WSGIApplication([
        ('%(rootpath)s/?' % {'rootpath' : rootpath}, MainBlogHandler),
        ('%(rootpath)s/newpost' % {'rootpath' : rootpath}, NewEntryHandler),
        ('%(rootpath)s/flush' % {'rootpath' : rootpath}, FlushHandler),
        ('%(rootpath)s/(\d+)' % {'rootpath' : rootpath}, BlogEntryHandler)],
        debug=True)
