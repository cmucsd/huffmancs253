'''
Udacity CS253 week 4 homework: login

Created on May 9, 2012

@author: cmaier
'''
import webapp2
import jinja2
import os
from google.appengine.ext import db

import random
import string
import hashlib

import logging

import re
#import cgi

rootpath='/hw_6'
template_dir= os.path.join(os.path.dirname(__file__),'templates')
jinja_env= jinja2.Environment(loader= jinja2.FileSystemLoader(template_dir), autoescape= True)

class User(db.Model):
    '''database item for user'''
    username= db.StringProperty(required= True)
    salt= db.StringProperty(required= True)
    pwhash= db.StringProperty(required= True)
    email= db.StringProperty()
    posted= db.DateTimeProperty(auto_now_add= True)

class Handler(webapp2.RequestHandler):
    '''some utility functions for page handlers on top of webapp2.RequestHandler'''
    def make_hash(self, pwd, salt= None):
        '''make hash for password'''
        if not salt:
            salt= self.make_salt()
        pwhash= hashlib.sha256(salt+pwd).hexdigest()
        #logging.info('pwhash=%s' % pwhash)
        return pwhash, salt
    def set_cookie(self, record, pwhash, path):
        cookieval= pwhash+str(record)
        #self.response.headers.add_header('Set-Cookie', 'user=%s' % str(uid))
        self.response.set_cookie('schtonk', cookieval, path=path)

    def write(self, *text, **kw):
        '''write text; default format is HTML'''
        self.response.out.write(*text, **kw)
    def render_str(self, template, **params):
        '''render Jinja template'''
        t= jinja_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        '''write Jinja template'''
        self.write(self.render_str(template, **kw))

class Signup(Handler):
    '''sign up handler'''
    validations= [re.compile(regexp) for regexp in (r"^[a-zA-Z0-9_-]{3,20}$", r"^.{3,20}$", r"^[\S]+@[\S]+\.[\S]+$")]
    def validate(self, s, regexp):
        '''validate form field entry'''
        return regexp.match(s)

    def check_user(self, uid):
        '''check if uid already exists'''
        #logging.info('userid: %s', uid)
        userquery= db.GqlQuery('SELECT * FROM User WHERE username = :1', uid)
        #logging.info('userquery: %s', userquery)
        users= [user.username for user in userquery]
        #logging.info('existing users: %s', users)
        return uid in users

    def make_salt(self, length= 8):
        return ''.join(random.choice(string.letters) for c in xrange(length))

    def register_user(self, uid, pwd, email):
        pwhash, salt= self.make_hash(pwd)
        user= User(username=uid, pwhash=pwhash, salt=salt, email=email) if email else User(username=uid, pwhash=pwhash, salt=salt)
        putty= user.put()
        return putty.id(), pwhash

    def signup_user(self, uid, pwd, email):
        '''sign up user'''
        record, pwhash= self.register_user(uid, pwd, email)
        self.set_cookie(record, pwhash, '/')
        self.redirect('%(rootpath)s/welcome' % {'rootpath' : rootpath})

    def invalid(self, uid, email, uid_valid, pwd_valid= True, pwd_match= True, email_valid= True):
        '''handle invalid form entry'''
        if isinstance(uid_valid, basestring):
            bad_uid= uid_valid
        else:
            bad_uid= "" if uid_valid else "That's not a valid username."
        bad_pwd= "" if pwd_valid else "That's not a valid password."
        pwd_mismatch= "" if pwd_match else "Your passwords didn't match."
        bad_email= "" if email_valid else "That's not a valid email."
        #self.response.headers['Content-Type'] = 'text/plain'
        self.render("signup.html", uid=uid, email=email, bad_uid=bad_uid, bad_pwd=bad_pwd, pwd_mismatch=pwd_mismatch, bad_email=bad_email)
        

    def get(self):
        '''handler for GET method'''
        self.render("signup.html")
    
    def post(self):
        '''handler for POST method'''
        (uid_in, pwd_in, pwd2_in, email_in)= [self.request.get(var) for var in ("username", "password", "verify", "email")]
        (uid_valid, pwd_valid, email_matches)= [self.validate(s, regexp) for (s, regexp) in zip((uid_in, pwd_in, email_in),self.validations)]
        email_valid= email_matches or email_in == ""
        pwd_match= pwd2_in == pwd_in
        #logging.info([uid_in,pwd_in,pwd2_in,email_in])
        (uid, email)= (uid_in, email_in)
        #logging.info([uid,email])
        if (uid_valid and pwd_valid and email_valid and pwd_match):
            if self.check_user(uid): 
                self.invalid(uid, email, "User already exists.")
            else:
                self.signup_user(uid, pwd_in, email)
        else:
            self.invalid(uid, email, uid_valid, pwd_valid, pwd_match, email_valid)

class Login(Handler):
    '''handler for login'''
    def check_user(self, uid, pwd):
        '''check if user exists and password is correct'''
        userquery= db.GqlQuery('SELECT * FROM User WHERE username = :1', uid)
        #logging.info(userquery)
        users= [user for user in userquery]
        if len(users) == 1:
            user= users[0]
            #logging.info('%s' % user)
            if user:
                #logging.info('uid: %s salt: %s hash: %s' % (user.username, user.salt, user.pwhash))
                loginhash= self.make_hash(pwd, user.salt)
                #logging.info('login   salt: %s hash: %s' % loginhash)
                if (user.pwhash, user.salt) == loginhash:
                    return user
            

    def get(self):
        '''handler for GET method'''
        self.render("login.html")

    def post(self):
        '''handler for POST method'''
        (uid, pwd)= [self.request.get(var) for var in ("username", "password")]
        user= self.check_user(uid, pwd)
        if user:
            self.set_cookie(user.key().id(), user.pwhash, '/')
            self.redirect('%(rootpath)s/welcome' % {'rootpath' : rootpath})
        else:
            self.render("login.html", uid=uid, bad_pwd= "Wrong user/password")

class Logout(Handler):
    '''handler for logout'''
    def get(self):
        self.set_cookie('', '', '/')
        self.redirect('%(rootpath)s/signup' % {'rootpath' : rootpath})
    pass

class PasswordAccepted(Handler):
    '''handler for accepted password'''
    def verify_cookie(self, cookieval):
        '''verify that cookie is correct'''
        if cookieval:
            pwhash= cookieval[:64]
            record= int(cookieval[64:])
            #logging.info('record: %s' % record)
            #logging.info('hash: %s' % pwhash)
            user= User.get_by_id(record)
            #logging.info(user)
            #logging.info(user.pwhash)
            if user.pwhash == pwhash:
                return user

    def get(self):
        cookieval= self.request.cookies.get('schtonk', '')
        #logging.info('cookie value: schtonk=%s' % cookieval)
        user= self.verify_cookie(cookieval)
        #logging.info("Record: %s" % user)
        if user:
        #self.response.headers['Content-Type'] = 'text/plain'
            #logging.info("Record: %s; user %s" % (user, user.username))
            self.render("welcome.html", uid= user.username)
        else:
            self.redirect("%(rootpath)s/signup" % {'rootpath' : rootpath})

strutten = webapp2.WSGIApplication([
    ('%(rootpath)s/signup' % {'rootpath' : rootpath}, Signup), 
    ('%(rootpath)s/login' % {'rootpath' : rootpath}, Login), 
    ('%(rootpath)s/logout' % {'rootpath' : rootpath}, Logout), 
    ('%(rootpath)s/pass' % {'rootpath' : rootpath}, PasswordAccepted), 
    ('%(rootpath)s/welcome' % {'rootpath' : rootpath}, PasswordAccepted)], 
    debug=True)
