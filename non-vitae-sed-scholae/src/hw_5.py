'''
Udacity CS253 week 5 homework: blog with JSON

Created on May 15, 2012

@author: cmaier
'''
import webapp2
import hw_3_1
import hw_4
from google.appengine.ext import db
import json

import logging

rootpath='/hw_6'

class EntryJsonHandler(hw_3_1.BlogEntryHandler):
    def headers(self):
        self.response.headers['Content-Type'] = 'application/json'
        self.response.headers['charset'] = 'UTF-8'
    def json_get(self, idstring):
        postid, blogentry= self.get_entry(idstring)
        entry= {
            'subject' : blogentry.subject,
            'content' : blogentry.content,
            'created' : blogentry.posted.strftime('%c'),
            'last_modified' : blogentry.last_modified.strftime('%c')}
        return entry
    
    def get(self, idstring):
        entry= self.json_get(idstring)
        dump= json.dumps(entry)
        logging.info('JSON object: %s' % repr(dump))
        self.headers()
        self.response.out.write(dump)

class BlogJsonHandler(EntryJsonHandler):
    def json_get_all(self):
        posts= db.GqlQuery('SELECT * from BlogEntry ORDER BY posted DESC')
        entries= [blogentry.key().id() for blogentry in posts]
        return [self.json_get(entry) for entry in entries]

    def get(self):
        entries= self.json_get_all()
        dump= json.dumps(entries)
        logging.info('JSON object: %s' % repr(dump))
        self.headers()
        self.response.out.write(dump)

schtonken = webapp2.WSGIApplication([
    ('%(rootpath)s/?' % {'rootpath' : rootpath}, hw_3_1.MainBlogHandler),
    ('%(rootpath)s/newpost' % {'rootpath' : rootpath}, hw_3_1.NewEntryHandler),
    ('%(rootpath)s/(\d+)' % {'rootpath' : rootpath}, hw_3_1.BlogEntryHandler),
    ('%(rootpath)s/signup' % {'rootpath' : rootpath}, hw_4.Signup), 
    ('%(rootpath)s/login' % {'rootpath' : rootpath}, hw_4.Login), 
    ('%(rootpath)s/logout' % {'rootpath' : rootpath}, hw_4.Logout), 
    ('%(rootpath)s/pass' % {'rootpath' : rootpath}, hw_4.PasswordAccepted), 
    ('%(rootpath)s/welcome' % {'rootpath' : rootpath}, hw_4.PasswordAccepted),
    ('%(rootpath)s/\.json' % {'rootpath' : rootpath}, BlogJsonHandler),
    ('%(rootpath)s/(\d+).json' % {'rootpath' : rootpath}, EntryJsonHandler)
    ], 
    debug=True)
