'''
Udacity CS253 week 6 homework: memory cache

Created on May 26, 2012

@author: cmaier
'''
import webapp2
import hw_3_1
import hw_4
import hw_5

from google.appengine.api import memcache
import logging

rootpath='/hw_6'

zappen = webapp2.WSGIApplication([
    ('%(rootpath)s/?' % {'rootpath' : rootpath}, hw_3_1.MainBlogHandler),
    ('%(rootpath)s/newpost' % {'rootpath' : rootpath}, hw_3_1.NewEntryHandler),
    ('%(rootpath)s/flush' % {'rootpath' : rootpath}, hw_3_1.FlushHandler),
    ('%(rootpath)s/(\d+)' % {'rootpath' : rootpath}, hw_3_1.BlogEntryHandler),
    ('%(rootpath)s/signup' % {'rootpath' : rootpath}, hw_4.Signup), 
    ('%(rootpath)s/login' % {'rootpath' : rootpath}, hw_4.Login), 
    ('%(rootpath)s/logout' % {'rootpath' : rootpath}, hw_4.Logout), 
    ('%(rootpath)s/pass' % {'rootpath' : rootpath}, hw_4.PasswordAccepted), 
    ('%(rootpath)s/welcome' % {'rootpath' : rootpath}, hw_4.PasswordAccepted),
    ('%(rootpath)s/\.json' % {'rootpath' : rootpath}, hw_5.BlogJsonHandler),
    ('%(rootpath)s/(\d+).json' % {'rootpath' : rootpath}, hw_5.EntryJsonHandler)
    ], 
    debug=True)